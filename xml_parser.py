from bs4 import BeautifulSoup
import os
from os import listdir
from os.path import isfile, join
import io

BASE_DIR = os.getcwd()

def split_num_to_string(text):
    '''
    Bu funksiya herf ve reqem eyni anda gelerse

    onlari bolerek 2 hisseye ayirmaga xidmet edir.

    '''
    string = ""
    number = ""
    for item in text:
        if item.isdigit():
            number += item
        else:
            string += item
    return [string, number]

def koton_xml_parser(xmlfiles):

    '''
    Bu funksiya koton firmasina aid olan pdf filelari pars etmek
    ucun istifade olunur.Pars olunmus melumatlari dictionary key,value olaraq bolub yazir.
    Bizde o keylere uygun olaraq excelle qeyd edirik datalari.

    Bu Funkiya 2 hisseden ibaretdir:
    1) id ye gore pars etmek.
    { Yeni birinci idni tapir sonra ona aid bboxlari tapir sonra onlara uygun text taginin icindeki datalari pars edir.}

    2) bboxsa gore pars etmek.
    { Yeni bboxlari qeyd edirik ve ona uygun gedib text taginin icindeki datalari  pars edir.}

    *********Bu id ve bboxlar her pdfde ayri-atri reqemlerden ibaretdiler.***********

    ps: qarsisinda "# *****her sirkete gore deyisen*******" bele comment gordukleriniz deyisen hisselerdi. 
    '''

    soup = BeautifulSoup(xmlfiles,'xml')
    authors = soup.find_all('textline')
    text_boxs = soup.find_all('textbox')

    result = {}
    for link in text_boxs: # Bu code idye gore pars edir.
        id_txtbox = link["id"]

        if id_txtbox == "6": # *****her sirkete gore deyisen*******

            i = 1
            text_line = link.find_all('textline')
            if len(text_line) > 1:
                i = 1
                for bbx in text_line:
                    base_item_list = []
                    item_list = ""
                    for item in bbx.find_all('text'):
                        if item.get("bbox"):
                            item_list += item.text
                        else:
                            base_item_list.append(item_list.strip())
                            item_list = ""
                    result[f"product_{i}"] = base_item_list[1:]
                    i+=1

        if id_txtbox == "10":  # *****her sirkete gore deyisen*******
            i = 1
            text_line = link.find_all('textline')
            if len(text_line) > 1:
                i = 1
                for bbx in text_line[2:]:
                    base_item_list = []
                    item_list = ""
                    for item in bbx.find_all('text'):
                        if item.get("bbox"):
                            item_list += item.text
                        else:
                            base_item_list.append(item_list.strip())
                            item_list = ""
                    result[f"product_{i}"] += base_item_list
                    i += 1

    for link in authors: # bu code bboxlara gore pars edir.
        cordinate = link["bbox"]

        if cordinate == "14.000,801.372,204.775,811.727": # Satıcının Adı-Soyadı/Ünvanı  # *****her sirkete gore deyisen*******
            result["title"] = "".join([x.text for x in link.find_all('text') if x.text != '\n'])
            
        elif cordinate == "14.000,726.396,81.519,736.365": # Satıcının Vergi Kimlik Numarası / TC Kimlik Numarası(0)  # *****her sirkete gore deyisen*******
            result["vkn"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]
            
        elif cordinate == "385.066,782.584,496.201,792.939": # Alış Faturasının Tarihi  # *****her sirkete gore deyisen*******
            result["tarih"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]
            
        elif cordinate == "385.066,767.151,529.080,777.505": # Alış Faturasının Serisi / Alış Faturasının Nosu  # *****her sirkete gore deyisen*******
            text = split_num_to_string("".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1])
            result["fatura_Serisi"] = text[0] # Alış Faturasının Serisi 
            result["fatura_No"] = text[1] # Alış Faturasının Nosu

        elif cordinate == "14.000,608.299,91.584,618.268": # Satıcının Vergi Kimlik Numarası / TC Kimlik Numarası(1)  # *****her sirkete gore deyisen*******
            result["tckn"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]

    return result

def polo_xml_parser(xmlfiles):
    '''
    Bu funksiya polo firmasina aid olan pdf filelari pars etmek
    ucun istifade olunur.Pars olunmus melumatlari dictionary key,value olaraq bolub yazir.
    Bizde o keylere uygun olaraq excelle qeyd edirik datalari.

    Bu Funkiya 2 hisseden ibaretdir:
    1) id ye gore pars etmek.
    { Yeni birinci idni tapir sonra ona aid bboxlari tapir sonra onlara uygun text taginin icindeki datalari pars edir.}

    2) bboxsa gore pars etmek.
    { Yeni bboxlari qeyd edirik ve ona uygun gedib text taginin icindeki datalari  pars edir.}

    *********Bu id ve bboxlar her pdfde ayri-atri reqemlerden ibaretdiler.***********

    ps: qarsisinda "# *****her sirkete gore deyisen*******" bele comment gordukleriniz deyisen hisselerdi.

    '''
    soup = BeautifulSoup(xmlfiles,'xml')
    authors = soup.find_all('textline')
    text_boxs = soup.find_all('textbox')

    
    result = {}
    for link in text_boxs:  
        id_txtbox = link["id"]

        if id_txtbox == "6": # *****her sirkete gore deyisen*******

            i = 1
            text_line = link.find_all('textline')

            if len(text_line) > 1:
                i = 1
            
                for bbx in text_line[3:-1]:
                    base_item_list = []
                    item_list = ""
                    for item in bbx.find_all('text'):
                        if item.get("bbox"):
                            item_list += item.text
                        else:
                            base_item_list.append(item_list.strip())
                            item_list = ""
                    if "." not in  base_item_list:
                        result[f"product_{i}"] = base_item_list
                    i+=1

    for link in authors:
        cordinate = link["bbox"]

        if cordinate == "24.605,806.061,118.409,814.010": # Satıcının Adı-Soyadı/Ünvanı  # *****her sirkete gore deyisen*******
            result["title"] = "".join([x.text for x in link.find_all('text') if x.text != '\n'])
            
        elif cordinate == "24.605,753.248,76.195,761.197": # Satıcının Vergi Kimlik Numarası / TC Kimlik Numarası(0)  # *****her sirkete gore deyisen*******
            result["vkn"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]
            
        elif cordinate == "372.094,630.816,470.497,638.936": # Alış Faturasının Tarihi  # *****her sirkete gore deyisen*******
            result["tarih"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]
            
        elif cordinate == "372.094,640.419,494.503,648.538": # Alış Faturasının Serisi / Alış Faturasının Nosu  # *****her sirkete gore deyisen*******
            text = split_num_to_string("".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1])
            result["fatura_Serisi"] = text[0] # Alış Faturasının Serisi 
            result["fatura_No"] = text[1] # Alış Faturasının Nosu

    return result

def defacto_xml_parser(xmlfiles):

    '''
    Bu funksiya defacto firmasina aid olan pdf filelari pars etmek
    ucun istifade olunur.Pars olunmus melumatlari dictionary key,value olaraq bolub yazir.
    Bizde o keylere uygun olaraq excelle qeyd edirik datalari.

    Bu Funkiya 2 hisseden ibaretdir:
    1) id ye gore pars etmek.
    { Yeni birinci idni tapir sonra ona aid bboxlari tapir sonra onlara uygun text taginin icindeki datalari pars edir.}

    2) bboxsa gore pars etmek.
    { Yeni bboxlari qeyd edirik ve ona uygun gedib text taginin icindeki datalari  pars edir.}

    *********Bu id ve bboxlar her pdfde ayri-atri reqemlerden ibaretdiler.***********

    ps: qarsisinda "# *****her sirkete gore deyisen*******" bele comment gordukleriniz deyisen hisselerdi. 

    '''
    soup = BeautifulSoup(xmlfiles,'xml')
    authors = soup.find_all('textline')
    text_boxs = soup.find_all('textbox')

    
    result = {}
    for link in text_boxs:  
        id_txtbox = link["id"]

        if id_txtbox == "4":# *****her sirkete gore deyisen*******
            i = 1
            text_line = link.find_all('textline')

            if len(text_line) > 1:
                i = 1
            
                for bbx in text_line[8:]:
                    base_item_list = []
                    item_list = ""
                    for item in bbx.find_all('text'):
                        if item.get("bbox"):
                            item_list += item.text
                        else:
                            base_item_list.append(item_list.strip())
                            item_list = ""
                        
                    if "." not in  base_item_list:
                        result[f"product_{i}"] = base_item_list
                    i+=1

    for link in authors:
        cordinate = link["bbox"]

        if cordinate == "33.508,791.066,122.997,800.090": # Satıcının Adı-Soyadı/Ünvanı  # *****her sirkete gore deyisen*******
            result["title"] = "".join([x.text for x in link.find_all('text') if x.text != '\n'])
            
        elif cordinate == "33.508,731.691,82.990,740.714": # Satıcının Vergi Kimlik Numarası / TC Kimlik Numarası(0)  # *****her sirkete gore deyisen*******
            result["vkn"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]
            
        elif cordinate == "292.917,627.351,387.362,636.568": # Alış Faturasının Tarihi  # *****her sirkete gore deyisen*******
            result["tarih"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]
            
        elif cordinate == "292.917,636.574,410.421,645.792": # Alış Faturasının Serisi / Alış Faturasının Nosu  # *****her sirkete gore deyisen*******
            text = split_num_to_string("".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1])
            result["fatura_Serisi"] = text[0] # Alış Faturasının Serisi 
            result["fatura_No"] = text[1] # Alış Faturasının Nosu

        elif cordinate == "34.085,606.598,91.061,615.622": # Satıcının Vergi Kimlik Numarası / TC Kimlik Numarası(1)  # *****her sirkete gore deyisen*******
            result["tckn"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]

    return result

def morhipo_xml_parser(xmlfiles):

    '''
    Bu funksiya morhipo firmasina aid olan pdf filelari pars etmek
    ucun istifade olunur.Pars olunmus melumatlari dictionary key,value olaraq bolub yazir.
    Bizde o keylere uygun olaraq excelle qeyd edirik datalari.

    Bu Funkiya 2 hisseden ibaretdir:
    1) id ye gore pars etmek.
    { Yeni birinci idni tapir sonra ona aid bboxlari tapir sonra onlara uygun text taginin icindeki datalari pars edir.}

    2) bboxsa gore pars etmek.
    { Yeni bboxlari qeyd edirik ve ona uygun gedib text taginin icindeki datalari  pars edir.}

    *********Bu id ve bboxlar her pdfde ayri-atri reqemlerden ibaretdiler.***********

    ps: qarsisinda "# *****her sirkete gore deyisen*******" bele comment gordukleriniz deyisen hisselerdi.

    '''
    soup = BeautifulSoup(xmlfiles,'xml')
    authors = soup.find_all('textline')
    text_boxs = soup.find_all('textbox')

    
    result = {}
    for link in text_boxs:  
        id_txtbox = link["id"]

        if id_txtbox == "8": # *****her sirkete gore deyisen*******

            i = 1
            text_line = link.find_all('textline')
            if len(text_line) > 1:
                i = 1
                for bbx in text_line:
                    base_item_list = []
                    item_list = ""
                    for item in bbx.find_all('text'):
                        if item.get("bbox"):
                            item_list += item.text
                        else:
                            base_item_list.append(item_list.strip())
                            item_list = ""
                    if result.get(f"product_{i}"):
                        result[f"product_{i}"] += base_item_list[1:]
                        i+=1
                    else:
                        result[f"product_{i}"] = base_item_list[1:]
                        i+=1


        if id_txtbox == "7":  # *****her sirkete gore deyisen*******

            i = 1
            text_line = link.find_all('textline')
            if len(text_line) > 1:
                i = 1
                for bbx in text_line[2:]:
                    base_item_list = []
                    item_list = ""
                    for item in bbx.find_all('text'):
                        if item.get("bbox"):
                            item_list += item.text
                        else:
                            base_item_list.append(item_list.strip())
                            item_list = ""
                    if result.get(f"product_{i}"):
                        result[f"product_{i}"] += base_item_list
                        i+=1
                    else:
                        result[f"product_{i}"] = base_item_list
                        i+=1

    for link in authors:
        cordinate = link["bbox"]

        if cordinate == "6.545,758.105,165.090,769.650": # Satıcının Adı-Soyadı/Ünvanı  # *****her sirkete gore deyisen*******
            result["title"] = "".join([x.text for x in link.find_all('text') if x.text != '\n'])
            
        elif cordinate == "6.545,697.415,60.005,706.113": # Satıcının Vergi Kimlik Numarası / TC Kimlik Numarası(0)  # *****her sirkete gore deyisen*******
            result["vkn"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]
            
        elif cordinate == "377.825,640.890,508.281,649.961": # Alış Faturasının Tarihi  # *****her sirkete gore deyisen*******
            result["tarih"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]
            
        elif cordinate == "377.825,649.220,533.021,658.291": # Alış Faturasının Serisi / Alış Faturasının Nosu  # *****her sirkete gore deyisen*******
            text = split_num_to_string("".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1])
            result["fatura_Serisi"] = text[0] # Alış Faturasının Serisi 
            result["fatura_No"] = text[1] # Alış Faturasının Nosu

    return result

def hm_xml_parser(xmlfiles):

    '''
    Bu funksiya HM firmasina aid olan pdf filelari pars etmek
    ucun istifade olunur.Pars olunmus melumatlari dictionary key,value olaraq bolub yazir.
    Bizde o keylere uygun olaraq excelle qeyd edirik datalari.

    Bu Funkiya 2 hisseden ibaretdir:
    1) id ye gore pars etmek.
    { Yeni birinci idni tapir sonra ona aid bboxlari tapir sonra onlara uygun text taginin icindeki datalari pars edir.}

    2) bboxsa gore pars etmek.
    { Yeni bboxlari qeyd edirik ve ona uygun gedib text taginin icindeki datalari  pars edir.}

    *********Bu id ve bboxlar her pdfde ayri-atri reqemlerden ibaretdiler.***********

    ps: qarsisinda "# *****her sirkete gore deyisen*******" bele comment gordukleriniz deyisen hisselerdi. 
    '''

    soup = BeautifulSoup(xmlfiles,'xml')
    authors = soup.find_all('textline')
    text_boxs = soup.find_all('textbox')

    result = {}
    for link in text_boxs: # Bu code idye gore pars edir.
        id_txtbox = link["id"]
        
        if id_txtbox == "9": # *****her sirkete gore deyisen*******  
            i = 1
            text_line = link.find_all('textline')
            if len(text_line) >= 1:
                i = 1
            
                for bbx in text_line:
                    base_item_list = []
                    item_list = ""
                    for item in bbx.find_all('text'):
                        if item.get("bbox"):
                            item_list += item.text
                        else:
                            base_item_list.append(item_list.strip())
                            item_list = ""
                    if "." not in  base_item_list:
                        result[f"product_{i}"] = base_item_list
                    i+=1
    for link in authors: # bu code bboxlara gore pars edir.
        cordinate = link["bbox"]

        if cordinate == "6.545,818.200,208.482,830.079": # Satıcının Adı-Soyadı/Ünvanı  # *****her sirkete gore deyisen*******
            result["title"] = "".join([x.text for x in link.find_all('text') if x.text != '\n'])
            
        elif cordinate == "6.545,736.685,56.896,745.272": # Satıcının Vergi Kimlik Numarası / TC Kimlik Numarası(0)  # *****her sirkete gore deyisen*******
            result["vkn"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]
            
        elif cordinate == "399.245,662.310,517.844,671.021": # Alış Faturasının Tarihi  # *****her sirkete gore deyisen*******
            result["tarih"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]
            
        elif cordinate == "399.245,671.235,541.838,679.946": # Alış Faturasının Serisi / Alış Faturasının Nosu  # *****her sirkete gore deyisen*******
            text = split_num_to_string("".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1])
            result["fatura_Serisi"] = text[0] # Alış Faturasının Serisi 
            result["fatura_No"] = text[1] # Alış Faturasının Nosu

        elif cordinate == "6.545,631.370,63.794,639.957": # Satıcının Vergi Kimlik Numarası / TC Kimlik Numarası(1)  # *****her sirkete gore deyisen*******
            result["tckn"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]

    return result

def kigili_xml_parser(xmlfiles):

    '''
    Bu funksiya kigili firmasina aid olan pdf filelari pars etmek
    ucun istifade olunur.Pars olunmus melumatlari dictionary key,value olaraq bolub yazir.
    Bizde o keylere uygun olaraq excelle qeyd edirik datalari.

    Bu Funkiya 2 hisseden ibaretdir:
    1) id ye gore pars etmek.
    { Yeni birinci idni tapir sonra ona aid bboxlari tapir sonra onlara uygun text taginin icindeki datalari pars edir.}

    2) bboxsa gore pars etmek.
    { Yeni bboxlari qeyd edirik ve ona uygun gedib text taginin icindeki datalari  pars edir.}

    *********Bu id ve bboxlar her pdfde ayri-atri reqemlerden ibaretdiler.***********

    ps: qarsisinda "# *****her sirkete gore deyisen*******" bele comment gordukleriniz deyisen hisselerdi. 
    '''

    soup = BeautifulSoup(xmlfiles,'xml')
    authors = soup.find_all('textline')
    text_boxs = soup.find_all('textbox')

    result = {}
    for link in text_boxs: # Bu code idye gore pars edir.
        id_txtbox = link["id"]

        if id_txtbox == "5": # *****her sirkete gore deyisen*******

            i = 1
            text_line = link.find_all('textline')
            if len(text_line) > 1:
                i = 1
                for bbx in text_line[1:]:
                    base_item_list = []
                    item_list = ""
                    for item in bbx.find_all('text'):
                        if item.get("bbox"):
                            item_list += item.text
                        else:
                            base_item_list.append(item_list.strip())
                            item_list = ""
                    result[f"product_{i}"] = base_item_list[1:]
                    i+=1
        if id_txtbox == "6":  # *****her sirkete gore deyisen*******
            i = 1
            text_line = link.find_all('textline')
            if len(text_line) > 1:
                i = 1
                for bbx in text_line[1:]:
                    base_item_list = []
                    item_list = ""
                    for item in bbx.find_all('text'):
                        if item.get("bbox"):
                            item_list += item.text
                        else:
                            base_item_list.append(item_list.strip())
                            item_list = ""        
                    result[f"product_{i}"] += base_item_list
                    i += 1

    for link in authors: # bu code bboxlara gore pars edir.
        cordinate = link["bbox"]

        if cordinate == "14.000,801.372,115.531,811.727": # Satıcının Adı-Soyadı/Ünvanı  # *****her sirkete gore deyisen*******
            result["title"] = "".join([x.text for x in link.find_all('text') if x.text != '\n'])
            
        elif cordinate == "14.000,758.605,81.519,768.573": # Satıcının Vergi Kimlik Numarası / TC Kimlik Numarası(0)  # *****her sirkete gore deyisen*******
            result["vkn"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]
            
        elif cordinate == "385.066,681.933,496.201,692.288": # Alış Faturasının Tarihi  # *****her sirkete gore deyisen*******
            result["tarih"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]
            
        elif cordinate == "385.066,666.500,528.409,676.855": # Alış Faturasının Serisi / Alış Faturasının Nosu  # *****her sirkete gore deyisen*******
            text = split_num_to_string("".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1])
            result["fatura_Serisi"] = text[0] # Alış Faturasının Serisi 
            result["fatura_No"] = text[1] # Alış Faturasının Nosu

    return result

def PR_xml_parser(xmlfiles):

    '''
    Bu funksiya pierrecardin firmasina aid olan pdf filelari pars etmek
    ucun istifade olunur.Pars olunmus melumatlari dictionary key,value olaraq bolub yazir.
    Bizde o keylere uygun olaraq excelle qeyd edirik datalari.

    Bu Funkiya 2 hisseden ibaretdir:
    1) id ye gore pars etmek.
    { Yeni birinci idni tapir sonra ona aid bboxlari tapir sonra onlara uygun text taginin icindeki datalari pars edir.}

    2) bboxsa gore pars etmek.
    { Yeni bboxlari qeyd edirik ve ona uygun gedib text taginin icindeki datalari  pars edir.}

    *********Bu id ve bboxlar her pdfde ayri-atri reqemlerden ibaretdiler.***********

    ps: qarsisinda "# *****her sirkete gore deyisen*******" bele comment gordukleriniz deyisen hisselerdi.

    '''

    soup = BeautifulSoup(xmlfiles,'xml')
    authors = soup.find_all('textline')
    text_boxs = soup.find_all('textbox')

    result = {}
    for link in text_boxs: # Bu code idye gore pars edir.
        id_txtbox = link["id"]

        if id_txtbox == "6": # *****her sirkete gore deyisen*******

            i = 1
            text_line = link.find_all('textline')
            if len(text_line) > 1:
                i = 1
                for bbx in text_line[3:]:
                    base_item_list = []
                    item_list = ""
                    for item in bbx.find_all('text'):
                        if item.get("bbox"):
                            item_list += item.text
                        else:
                            base_item_list.append(item_list.strip())
                            item_list = ""
                    if "." not in  base_item_list:
                        result[f"product_{i}"] = base_item_list
                    i+=1 

    for link in authors: # bu code bboxlara gore pars edir.
        cordinate = link["bbox"]

        if cordinate == "24.605,806.061,118.409,814.010": # Satıcının Adı-Soyadı/Ünvanı  # *****her sirkete gore deyisen*******
            result["title"] = "".join([x.text for x in link.find_all('text') if x.text != '\n'])
            
        elif cordinate == "24.605,753.248,76.195,761.197": # Satıcının Vergi Kimlik Numarası / TC Kimlik Numarası(0)  # *****her sirkete gore deyisen*******
            result["vkn"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]
            
        elif cordinate == "372.094,683.029,470.497,691.149": # Alış Faturasının Tarihi  # *****her sirkete gore deyisen*******
            result["tarih"] = "".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1]
            
        elif cordinate == "372.094,692.632,494.503,700.752": # Alış Faturasının Serisi / Alış Faturasının Nosu  # *****her sirkete gore deyisen*******
            text = split_num_to_string("".join([x.text for x in link.find_all('text') if x.text != '\n']).split(": ")[-1])
            result["fatura_Serisi"] = text[0] # Alış Faturasının Serisi 
            result["fatura_No"] = text[1] # Alış Faturasının Nosu

    return result
