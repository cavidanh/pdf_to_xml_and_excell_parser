try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import XMLConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
import os
import sys, getopt
from os import listdir
from io import BytesIO
from os.path import isfile, join
import io

BASE_DIR = os.getcwd()
def convert(fname, pages=None):
    '''
    Bu funksiya pdf fileni parse edir ve return edir.
    '''
    if not pages:
        pagenums = set()
    else:
        pagenums = set(pages)
        
    
    rsrcmgr = PDFResourceManager()
    retstr = BytesIO()
    laparams = LAParams(char_margin=30.0)
    codec='utf-8'
    converter = XMLConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    interpreter = PDFPageInterpreter(rsrcmgr, converter)

    file_type = fname.split(".")[-1]

    if file_type == "pdf" or file_type == "PDF":
        infile = open(fname, 'rb')
        for page in PDFPage.get_pages(infile, pagenums):
            interpreter.process_page(page)
        infile.close()
        converter.close()
        text = retstr.getvalue().decode()
        retstr.close
        return text 

def convertMultiple(pdfDir):
    '''
    Bu funksiya ise convert() funksiyasini cagiraraq parse olunmus

    melumatlari txt file yazib deqiq path e save edir.
    '''
    if pdfDir == "": 
        pdfDir = os.getcwd() + "\\" 
    for pdf in os.listdir(pdfDir):
        fileExtension = pdf.split(".")[-1]
        if fileExtension == "pdf" or fileExtension == "PDF":
            pdfFilename = pdfDir + pdf 
            text = convert(pdfFilename)
            if pdf.split(".")[-1] == "PDF":
                textFilename = pdf.replace('.PDF','.xml')
            elif pdf.split(".")[-1] == "pdf":
                textFilename = pdf.replace('.pdf','.xml')
                
        with open(os.path.join(BASE_DIR, "XML_Folder", textFilename),"w") as textFile:
            textFile.write(text) # txt filenin deqiq pathine save prosesi burda gedir.

pdfDir = os.path.join(BASE_DIR, "Pdf_Folder") + "/" # pdf filenin deqiq pathi burda verilib. 
convertMultiple(pdfDir)
